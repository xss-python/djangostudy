
from django.shortcuts import redirect,render,HttpResponse

from django.views.decorators.cache import cache_page


@cache_page(timeout=10)
def page_cache(request):
    print("-----------------cache example----------------")
    import time
    ctime=time.time()
    return render(request,'cache_test.html',{'ctime':ctime})
