from django.shortcuts import render

# Create your views here.

from django.shortcuts import redirect,render,HttpResponse
from django.views.decorators.csrf import csrf_exempt,csrf_protect
from signaltest.defined_cust_signal import log_signal
def user_session_auth(func):
    """
    用户session 认证的装饰器
    :param func: 
    :return: 
    """
    def inner(request,*args,**kwargs):
        user_info= request.session.get('userinfo')
        print("----filter---- user_info=%s" %user_info)
        if not user_info:
            return redirect('/user/list/')
        return func(request,*args,**kwargs)
    return inner

@user_session_auth
def user_index(request):
    return render(request,'user_index.html')

def user_list(request):
    print("----in user list-----")
    log_signal.send(sender="system",msg="xxxxtest messagexxxxxxx")
    return render(request,'user_list.html')

def user_before_add(request):
    return render(request, 'user_add.html')

@csrf_protect
def user_add(request):
    print("-----in user add-----")
    print(request.session)
    print(dir(request.session))

    username=request.POST.get('username')
    pwd=request.POST.get('pasword')
    print("-----in user add username=%s,password=%s-----" %(username,pwd))

    user_info={'username':username,'id':'1000'}
    request.session['userinfo']=user_info

    print(request.session.get('userinfo'))

    return redirect('/user/index/')

###############################form module##########################
from django import forms
from django.forms import fields

class UserDetail(forms.Form):
    username=fields.CharField(
        label="username:",
        error_messages={'required':'username is required!!!'}
    )
    password=fields.CharField(
        label='密码:',
        max_length=16,
        min_length=6
    )
    email=fields.EmailField(
        label='email:',
        required=False
    )
    age=fields.IntegerField(
        label='Age:'
    )

def user_detail(request):
    if request.method == 'GET':
        obj=UserDetail()
        print(obj.as_p())
        return render(request,'user_detail.html',{'obj':obj})
    else:
        obj=UserDetail(request.POST)
        validate=obj.is_valid()
        print(validate)
        if validate :
            print(obj.cleaned_data)
            

        return render(request,'user_detail.html',{'obj':obj})

