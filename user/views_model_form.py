
from user.models import UserGroup,UserInfo
from django.shortcuts import HttpResponse,redirect,render
from django.db import transaction
"""
基于ModelForm操作表单,将form model与操作数据库的model整合成一个

"""

from  django import forms
from django.forms import widgets as formWidgets

class UserModelForm(forms.ModelForm):

    class Meta:
        model=UserInfo
        fields='__all__'
        labels={
            'username':'用户名:',
            'password':'密码:',
            'nickname':'别名:',
            'group':'用户组:',
            'mobile':'手机号:',
            'status':'状态:',
        }
        widgets={
            'password':formWidgets.PasswordInput
        }

    def __init__(self,*args,**kwargs):
        super(UserModelForm, self).__init__(*args,**kwargs)
        print('-----model form init---------')
        print(self.fields['group'])
        self.fields['group'].choices=UserGroup.objects.values_list('id','name')

def add_user(request):
    if request.method == 'GET':
        uid=request.GET.get('uid')
        if uid:
            user_obj=UserInfo.objects.filter(id=uid).first()
            # UserInfo.objects.raw("select * from user where id=0") #原生SQL的方式执行
            if user_obj:
                print("find user obj=",user_obj.password)
                obj = UserModelForm(instance=user_obj)
        else:
            obj=UserModelForm()
        print('------user model get method obj=',obj)
        return render(request,'user_model_test.html',{'obj':obj})

    elif request.method == 'POST':
        obj = UserModelForm(request.POST)
        print(obj.is_valid())
        if obj.is_valid():
            print(obj.cleaned_data)
            #以事务方式提交
            with transaction.atomic():
                obj.save()
        else:
            print(obj.errors.as_json())
        return render(request, 'user_model_test.html', {'obj': obj})