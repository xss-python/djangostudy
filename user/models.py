from django.db import models

# Create your models here.
"""
默认情况下根据模型来创建表

"""
class UserGroup(models.Model):
    name=models.CharField(max_length=30,blank=False)
    type=models.IntegerField(default=1,blank=False)

class UserInfo(models.Model):
    username = models.CharField(max_length=30, blank=False)
    password = models.CharField(max_length=20, blank=False)
    nickname = models.CharField(max_length=30, blank=True)
    mobile = models.CharField(max_length=13, blank=False)
    status = models.IntegerField(default=1)
    group = models.ManyToManyField(UserGroup)