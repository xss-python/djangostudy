from django.shortcuts import render
from blog import  models
# Create your views here.



def list(request,*args,**kwargs):
    # print(args)
    # print(kwargs)
    condition={}
    for k,v in kwargs.items():
        kwargs[k]=int(v)
        if v == '0':
            pass
        else:
            condition[k]=v

    article_type_list=models.Article.type_choice
    print(article_type_list)
    category_list=models.Category.objects.all()
    result=models.Article.objects.filter(**condition)

    return render(request,'blog_list.html',{
        'result':result,
        'article_type_list':article_type_list,
        'category_list':category_list,
        'arg_dict':kwargs
    })