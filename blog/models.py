from django.db import models

# Create your models here.

class Category(models.Model):
    caption=models.CharField(max_length=30)

class Article(models.Model):
    title=models.CharField(max_length=50)
    content=models.CharField(max_length=255)
    category=models.ForeignKey(to=Category,on_delete=models.CASCADE)

    type_choice=(
        (1,'Java'),
        (2,'Python'),
        (3,'Go'),
        (4,'ES'),
    )
    article_type_id=models.IntegerField(choices=type_choice)
