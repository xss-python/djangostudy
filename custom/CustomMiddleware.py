from django.utils.deprecation import MiddlewareMixin

class log_filter(MiddlewareMixin):
    def process_request(self, request):
        print("-----log filter reqeust-----------")

    def process_view(self, request, view_func, view_func_args, view_func_kwargs):
        print("-----log filter view-----------")

    def process_response(self, request, response):
        print("-----log filter response-----------")
        return response
