from django.core.signals import request_finished
from django.core.signals import request_started
from django.dispatch import receiver

print("****init request signal callback with annotion****")


@receiver(request_started)
def reqquest_callback(sender,**kwargs):
    print("another request started callback sender=%s" %sender)


#自定义的信号
from signaltest.defined_cust_signal import  log_signal

@receiver(log_signal)
def log_callback(sender,**kwargs):
    print("*****log callback****** sender=%s" %sender)
    print("*****log callback****** args=%s" %kwargs['msg'])





