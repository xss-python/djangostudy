from django.core.signals import request_started
from django.core.signals import request_finished

print("****init request signal callback ****")

def request_start_callback(sender,**kwargs):
    print("request callback sender=%s" %sender)
    print("request callback args=%s" %kwargs);


def request_finished_callback(sender,**kwargs):
    print("request finished callback sender=%s" %sender)
    print("request finished callback args=%s" %kwargs)


request_started.connect(request_start_callback) #请求开始之后，注册信号回调可以注册多个

# request_finished.connect(request_finished_callback)
